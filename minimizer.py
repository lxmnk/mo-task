#!/usr/bin/env python
import os
import time
from collections.abc import Iterable
from concurrent.futures import ProcessPoolExecutor
from copy import deepcopy
from datetime import datetime as dt
from itertools import combinations
from multiprocessing import cpu_count
from pprint import pprint
from random import random, sample

from shapely.geometry import Point, box
from shapely.ops import cascaded_union

EPSILON = 10 ** -6
N_CORES = cpu_count()
LOG_FILES_DIR = 'logs'
LOG_FILENAME = None


class Cell:
    """Клетка на карте нефти, имеющая собственную скорость добычи

    Свойства:
        polygon (shapely.geometry.Polygon): для поиска пересечений,
            вычисления площади и т.д.
        oil (float): скорость добычи нефти в ячейке
        poro (float): рост радиуса скважины в ячейке за единицу времени
    """

    def __init__(self, column, line, cell_width, cell_height, oil, poro):
        x_min = column * cell_width
        x_max = (column + 1) * cell_width
        y_min = line * cell_height
        y_max = (line + 1) * cell_height
        self.polygon = box(x_min, y_min, x_max, y_max)
        self.oil = oil
        self.poro = poro


class OilMap:
    """Матрица, состоящая из ячеек класса `Cell`

    Конкретную ячейку можно получить так:
    >>> oil_map[x, y]
    <minimizer.Cell at 0x...>
    """

    def __init__(self, oil_filename, poro_filename):
        map_properties1 = self.read_file_header(oil_filename, 'OIL')
        map_properties2 = self.read_file_header(poro_filename, 'PORO')
        assert [p1 == p2 for p1, p2 in zip(map_properties1, map_properties2)]
        map_width, map_height, cell_width, cell_height = map_properties1
        oil_data = self.read_file_data(oil_filename)
        poro_data = self.read_file_data(poro_filename)
        assert len(oil_data) == len(poro_data) == map_width * map_height
        self.map_width = map_width
        self.map_height = map_height
        self.cell_width = cell_width
        self.cell_height = cell_height
        # В данной функции используются 4 свойства, заданных выше.
        self.matrix = self.fill_matrix(oil_data, poro_data)

    def fill_matrix(self, oil_data, poro_data):
        matrix = []
        data_iter = iter(zip(oil_data, poro_data))
        # Порядок заполнения ячеек матрицы:
        # 7 8 9
        # 4 5 6
        # 1 2 3
        for i in range(self.map_height):
            matrix.append([])
            for j in range(self.map_width):
                oil, poro = next(data_iter)
                c = Cell(j, i, self.cell_width, self.cell_height, oil, poro)
                matrix[i].append(c)
        return matrix

    def __getitem__(self, xy):
        i, j = xy
        return self.matrix[j][i]

    def read_file_header(self, filename, filetype):
        """Тип файла должен быть равен 'OIL' или 'PORO'."""
        with open(filename, 'r') as f:
            assert f.readline().strip() == filetype
            map_width, map_height = map(int, f.readline().strip().split())
            cell_width, cell_height = map(int, f.readline().strip().split())
        return map_width, map_height, cell_width, cell_height

    def read_file_data(self, filename):
        data = []
        with open(filename, 'r') as f:
            f.readline()  # Пропускаем три
            f.readline()  # строки с
            f.readline()  # шапкой
            line = f.readline().strip()
            while line != '/':
                data.append(float(line))
                line = f.readline().strip()
        return data


class OilWell:
    """Объект скважины, хранящий её текущее состояние"""

    def __init__(self, x, y, r, oil_map):
        self.x = x
        self.y = y
        self.r = r
        self.poro = self._find_poro(x, y, oil_map)
        self.polygon = Point(x, y).buffer(r)

    def _find_poro(self, x, y, oil_map):
        """Возвращает значение PORO на карте по заданным (x, y)"""
        if not (
            0 <= x < oil_map.map_width * oil_map.cell_width
            and 0 <= y < oil_map.map_height * oil_map.cell_height
        ):
            return 0
        x_cell = int(x) // oil_map.cell_width
        y_cell = int(y) // oil_map.cell_height
        return oil_map[x_cell, y_cell].poro

    def update(self):
        self.r += self.poro
        self.polygon = Point(self.x, self.y).buffer(self.r)

    def get_oil(self, oil_map):
        return get_overlapped_oil(
            self.polygon, get_intersected_cells(self.polygon, oil_map)
        )


class OilWellsGroup:
    """Коллекция скважин для упрощения подсчёта добытой нефти"""

    def __init__(self, oil_wells_iterable):
        assert isinstance(oil_wells_iterable, Iterable)
        self.oil_wells = oil_wells_iterable

    def update(self):
        for o in self.oil_wells:
            o.update()

    def get_oil(self, oil_map, concurrency):
        circles = [o.polygon for o in self.oil_wells]
        return get_oil_by_each_oil_well(circles, oil_map, concurrency)


def get_intersected_cells(polygon, oil_map):
    if not polygon.bounds:
        return []
    x_min, y_min, x_max, y_max = [int(x) for x in polygon.bounds]
    i_min = y_min // oil_map.cell_height
    i_max = y_max // oil_map.cell_height + 1
    j_min = x_min // oil_map.cell_width
    j_max = x_max // oil_map.cell_width + 1
    intersected = []
    for i in range(i_min, i_max):
        for j in range(j_min, j_max):
            if (
                0 <= i < oil_map.map_height
                and 0 <= j < oil_map.map_width
                and polygon.intersects(oil_map[j, i].polygon)
            ):
                intersected.append(oil_map[j, i])
    return intersected


def get_overlapped_oil(polygon, intersected):
    return sum(
        [
            polygon.intersection(cell.polygon).area * cell.oil
            for cell in intersected
        ]
    )


def get_oil_by_each_oil_well(circles, oil_map, concurrency):
    """Возвращает список из кол-ва нефти, собранной каждым кругом

       Для получения добытой нефти для каждой из скважин нужно
       просуммировать добытую нефть тех пересечений, в которые входит
       круг. Важно не забывать делить количество нефти в пересечении на
       уровень пересечения.
    """
    intersections = get_circles_intersections(circles)
    first_level_len = len([x for x in intersections if len(x) == 1])
    cells_by_intersections = {
        k: get_intersected_cells(v, oil_map) for k, v in intersections.items()
    }
    if concurrency:
        with ProcessPoolExecutor(max_workers=N_CORES) as executor:
            futures = [
                executor.submit(
                    get_overlapped_oil, v, cells_by_intersections[k]
                )
                for k, v in intersections.items()
            ]
            oil_by_intersections = {
                k: v.result() for k, v in zip(intersections, futures)
            }
    else:
        oil_by_intersections = {
            k: get_overlapped_oil(v, cells_by_intersections[k])
            for k, v in intersections.items()
        }
    return [
        sum(
            [
                oil / len(x)
                for x, oil in oil_by_intersections.items()
                if circle in x
            ]
        )
        for circle in range(1, first_level_len + 1)
    ]


def get_circles_intersections(circles):
    """Находит все пересечения между кругами

       Введём собственное понятие пересечения. Пересечение - это пара
       ключ-значение, где ключ - множество из номеров пересекающихся
       кругов, а значение - полигон, занимаемый этим пересечением.

       Назовём пересечением первого уровня множество из одного элемента.
       Это часть круга (или весь круг), которая не пересекает никакой
       другой круг. Пересечения второго уровня и выше содержат в себе
       несколько номеров кругов.

       Пример:
       >>> pprint(intersections)
           {frozenset({1}): Polygon,
            frozenset({2}): Polygon,
            frozenset({3}): Polygon,
            frozenset({1, 2}): Polygon,
            frozenset({2, 3}): Polygon,
            frozenset({1, 3}): Polygon,
            frozenset({1, 2, 3}): Polygon}

       Для заполнения данного словаря используется следующий алгоритм:
       1. Двигаемся по кругам, сравнивая их попарно. Если круги
          пересекаются, создаём пересечение второго уровня.
       2. Двигаемся по уровням, начиная от второго:
           * Сравнивая попарно пересечения в уровне, ищем пары со всеми
             одинаковыми элементами, кроме одного. Они могут
             образовывать пересечение следующего уровня. Например,
             {1, 2} и {2, 3} => {1, 2, 3}. Если нашлось такое пересечение,
             добавляем его в словарь и находим его полигон.
           * Вычитаем полигоны всех созданных пересечений нового уровня
             из пересечений предыдущего уровня.
       3. Двигаемся по кругам. Из каждого круга вычитаем все
          пересечения, в которых он содержится и создаём пересечение
          первого уровня.
    """
    intersections = find_second_level_intersections(circles)
    intersections = add_upper_levels_intersections(intersections)
    first_level = {
        frozenset([k]): v for k, v in zip(range(1, len(circles) + 1), circles)
    }
    return add_first_level_intersections(first_level, intersections)


def find_second_level_intersections(circles):
    intersections = {}
    for a, b in combinations(circles, 2):
        if a.intersects(b):
            c = frozenset({circles.index(a) + 1, circles.index(b) + 1})
            intersections[c] = a.intersection(b)
    return intersections


def add_upper_levels_intersections(intersections):
    level = intersections.copy()
    n_level = 2
    while len(level) > 1:
        for a, b in combinations(level, 2):
            c = frozenset(a | b)
            if (
                len(c - a) == 1
                and c not in intersections
                and level[a].intersects(level[b])
            ):
                polygon = intersections[a].intersection(intersections[b])
                intersections[c] = polygon
        n_level += 1
        old_level = level.copy()
        level = {k: v for k, v in intersections.items() if len(k) == n_level}
        level_diffs = cascaded_union(level.values())
        for x in old_level:
            # .buffer() используется на случай, если внутри объединения
            # появятся линии. Эти линии создают баг, из-за которого
            # операция difference выдаёт неверный результат.
            intersections[x] = (
                intersections[x].difference(level_diffs).buffer(EPSILON)
            )
    return intersections


def add_first_level_intersections(first_level, intersections):
    for circle in first_level:
        # Смотри комментарий к .buffer() в add_upper_levels_intersections.
        circle_diffs = cascaded_union(
            [v for k, v in intersections.items() if circle <= k]
        ).buffer(EPSILON)
        first_level[circle] = first_level[circle].difference(circle_diffs)
    intersections.update(first_level)
    return intersections


def get_total_oil_wells_oil(circles, oil_map):
    polygon = cascaded_union(circles)
    return get_overlapped_oil(polygon, get_intersected_cells(polygon, oil_map))


def init_ga(n_oil_wells, r, oil_map):
    x_max = oil_map.map_width * oil_map.cell_width
    y_max = oil_map.map_height * oil_map.cell_height
    return [
        OilWell(random() * x_max, random() * y_max, r, oil_map)
        for _ in range(n_oil_wells)
    ]


def mutate(x, y, oil_map, mutation_force):
    number = random()
    # По 0.4, что поменяется один из генов, и 0.2 - что оба.
    if number <= 0.4:
        x += random() * (
            oil_map.map_width * oil_map.cell_width * mutation_force
        )
    elif number <= 0.8:
        y += random() * (
            oil_map.map_height * oil_map.cell_height * mutation_force
        )
    else:
        x += random() * oil_map.map_width * mutation_force
        y += random() * oil_map.map_height * mutation_force
    return x, y


def create_offsprings(
    generation, oil_map, mutation_chance=0.05, mutation_force=0.1
):
    offsprings = []
    for p1, p2 in sample(list(combinations(generation, 2)), len(generation)):
        # Согласно литературе по генетическим алгоритмам, [−0.25, 1.25]
        # считается самым удачным промежутком для мутации.
        x_mult, y_mult = [(random() * 1.5 - 0.25) for _ in range(2)]
        child_x = p1.x + x_mult * (p2.x - p1.x)
        child_y = p1.y + y_mult * (p2.y - p1.y)
        if random() <= mutation_chance:
            child_x, child_y = mutate(
                child_x, child_y, oil_map, mutation_force
            )
        offsprings.append(OilWell(child_x, child_y, p1.r, oil_map))
    return offsprings


def get_oil_by_each_oil_well_for_all_iterations(
    generation, n_poro_iterations, oil_map, concurrency
):
    generation_copy = deepcopy(generation)
    oil_wells_group = OilWellsGroup(generation_copy)
    total_list = oil_wells_group.get_oil(oil_map, concurrency)
    for _ in range(n_poro_iterations):
        oil_wells_group.update()
        total_list = list(
            map(
                sum,
                zip(total_list, oil_wells_group.get_oil(oil_map, concurrency)),
            )
        )
    return total_list


def get_oil_for_all_iterations(oil_well, n_poro_iterations, oil_map):
    oil_well_copy = deepcopy(oil_well)
    total = oil_well_copy.get_oil(oil_map)
    for _ in range(n_poro_iterations):
        oil_well_copy.update()
        total += oil_well_copy.get_oil(oil_map)
    return total


def make_natural_selection(union_of_generations, n_poro_iterations, oil_map):
    half = len(union_of_generations) // 2
    go_on = True
    while len(union_of_generations) != half and go_on:
        for o in union_of_generations:
            if not (
                0 <= o.x < oil_map.map_width * oil_map.cell_width
                and 0 <= o.y < oil_map.map_height * oil_map.cell_height
            ):
                union_of_generations.remove(o)
                break
        else:
            go_on = False
    go_on = True
    while len(union_of_generations) != half and go_on:
        for a, b in combinations(union_of_generations, 2):
            distance_lim = (
                (a.r + a.poro * n_poro_iterations)
                + (b.r + a.poro * n_poro_iterations)
            ) * (2 / 3)
            if distance_lim > a.polygon.centroid.distance(b.polygon.centroid):
                union_of_generations.remove(a)
                break
        else:
            go_on = False
    if len(union_of_generations) != half:
        results = [
            get_oil_for_all_iterations(oil_well, n_poro_iterations, oil_map)
            for oil_well in union_of_generations
        ]
        while len(union_of_generations) != half:
            index = results.index(min(results))
            results.pop(index)
            union_of_generations.pop(index)
    return union_of_generations


def log_best_result(best_result, best_gen_number, best_generation):
    global LOG_FILENAME
    if LOG_FILENAME is None:
        LOG_FILENAME = dt.now().isoformat(timespec='seconds') + '.log'
    if not os.path.isdir(LOG_FILES_DIR):
        os.mkdir(LOG_FILES_DIR)
    with open(os.path.join(LOG_FILES_DIR, LOG_FILENAME), 'w') as f:
        f.write(f'Best result: {best_result}\n')
        f.write(f'Nubmer of generation: {best_gen_number}\n')
        pprint([(o.x, o.y) for o in best_generation], f)


def find_oil_wells_coordinates_using_ga(
    n_oil_wells,
    n_poro_iterations,
    r,
    oil_map,
    last_generation=None,
    max_time=None,
    logging=True,
    concurrency=False,
):
    """Реализация генетического алгоритма.

    1) Начинаем отсчёт времени, если необходимо.
    2) Генерируем первое поколение со случайными координатами для каждой
       из вышек. Назовём его "старым поколением" и установим кол-во
       итераций равным 0.
    3) Подсчитываем, сколько нефти добыло старое поколение за все
       итерации и запоминаем это число. На данный момент это число будет
       лучшим результатом (поколение тоже будет лучшим -- добывшим самое
       большое кол-во нефти).
    4) В цикле (управляемом за счёт кол-ва итераций и/или доступного на
       рассчёты времени):
       а) Если цикл управляется кол-вом итераций и они закончились --
          прерываем его.
       б) Увеличиваем популяцию в 2 раза за счёт скрещивания вышек между
          собой, используя промежуточную рекомбинацию (так же известную
          как дифференциональное скрещивание). С вероятностью 1/20
          применям мутацию к потомкам, добавляя смещение в случайную
          сторону от 0 до 1/10 от размера карты. Вероятность добавления
          смещения к каждому из двух генов равна 0.4, а вероятность
          добавления смещения к обоим генам -- 0.2.
       в) Проводим естественный отбор, уменьшая количество особей до
          исходного, используя три алгоритма:
          1. Вышка уничтожается, если вышла за пределы карты.
          2. Вышка уничтожается, если её расстояние до любой другой
             вышки меньше, чем (r1 + r2) * 2/3.
          3. Вышка уничтожается, если количество добытой ею нефти
             является самым плохим результатом.
          Алгоритмы применяются по очереди. Второй включается в работу
          только тогда, когда первый уже не может удалить ни одну вышку,
          а количество вышек всё ещё превышает исходное. Аналогично для
          третьего.
       г) Подсчитываем, сколько нефти выкачало новое поколение за все
          итерациии.
       д) Если критерием остановки является время и оно вышло --
          прерываем его. Проверка на время находится здесь, а не в
          начале цикла потому, что если время превышено, то полученный в
          текущей итерации результат не может являться ответом.
       е) Перезаписываем старое поколение новым.
       ж) Если старое (перезаписанное) поколение имеет лучший результат,
          то обновляем этот результат и делаем это поколение лучшим.
       з) Выполняем икремент кол-ва итераций.
    5) Возвращаем лучшее поколение и добытую им нефть как результат.
    """
    assert last_generation is not None or max_time is not None
    counter = 0
    if max_time is not None:
        start = time.time()
    old_generation = init_ga(n_oil_wells, r, oil_map)
    old_result = sum(
        get_oil_by_each_oil_well_for_all_iterations(
            old_generation, n_poro_iterations, oil_map, concurrency
        )
    )
    best_generation = old_generation
    best_result = old_result
    best_gen_number = counter
    while True:
        if counter == last_generation:
            break
        offsprings = create_offsprings(old_generation, oil_map)
        new_generation = make_natural_selection(
            old_generation + offsprings, n_poro_iterations, oil_map
        )
        new_result = sum(
            get_oil_by_each_oil_well_for_all_iterations(
                new_generation, n_poro_iterations, oil_map, concurrency
            )
        )
        if max_time is not None and time.time() - start > max_time:
            break
        old_generation = new_generation
        old_result = new_result
        if old_result > best_result:
            best_result = old_result
            best_generation = old_generation
            best_gen_number = counter + 1  # Считаем номера с 1.
        if logging:
            log_best_result(best_result, best_gen_number, best_generation)
        counter += 1
    return best_result, best_gen_number, [(o.x, o.y) for o in best_generation]


def main():
    pass


if __name__ == '__main__':
    main()
