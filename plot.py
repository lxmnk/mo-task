import os
from random import choice

from descartes import PolygonPatch
from matplotlib import pyplot
from shapely.geometry.collection import GeometryCollection
from shapely.geometry.multipolygon import MultiPolygon
from shapely.geometry.polygon import Polygon


def create_subplot_to_draw(oil_map, grid):
    figure = pyplot.figure()
    subplot = figure.add_subplot(111)
    map_width, map_height = oil_map.dimensions
    subplot.set_xlim([0, map_width * oil_map.cell_width])
    subplot.set_ylim([0, map_height * oil_map.cell_height])
    subplot.set_aspect('equal')
    subplot.grid(grid)
    return subplot


def get_color_alpha(value, max_value):
    if value == 0:
        return 0.001
    return value / max_value


def get_max_oil_value(oil_map):
    map_width, map_height = oil_map.dimensions
    return max(
        (
            oil_map[i, j].oil_value
            for i in range(map_width)
            for j in range(map_height)
        )
    )


def get_polygons(collection, accumulator=None):
    if accumulator is None:
        accumulator = []
    for x in collection:
        if isinstance(x, GeometryCollection):
            accumulator = get_polygons(x, accumulator)
        if isinstance(x, MultiPolygon) or isinstance(x, Polygon):
            accumulator.append(x)
    return accumulator


def get_random_color():
    return choice(
        [
            '#fb4934',  # red
            '#b8bb26',  # green
            '#fabd2f',  # yellow
            '#83a598',  # blue
            '#d3869b',  # purple
            '#8ec07c',  # aqua
            '#ebddb2',  # white
            '#fe8019',  # orange
        ]
    )


def draw_cells(oil_map, subplot):
    max_oil_value = get_max_oil_value(oil_map)
    map_width, map_height = oil_map.dimensions
    for i in range(map_width):
        for j in range(map_height):
            alpha = get_color_alpha(oil_map[i, j].oil_value, max_oil_value)
            draw_polygon(
                oil_map[i, j].polygon,
                subplot,
                alpha=alpha,
                color='#a89984',
                ec='#928374',
            )


def draw_polygon(polygon, subplot, alpha=None, color=None, ec=None, text=None):
    patch = PolygonPatch(
        polygon,
        alpha=(alpha or 0.4),
        color=(color or get_random_color()),
        ec=(ec or '#282828'),
        zorder=1,
    )
    subplot.add_patch(patch)
    x_min, y_min, x_max, y_max = polygon.bounds
    if text is not None:
        pyplot.text(
            x_min + (x_max - x_min) / 2,
            y_min + (y_max - y_min) / 2,
            text,
            horizontalalignment='center',
            verticalalignment='center',
        )


def draw_polygons(polygons, subplot):
    for i, p in enumerate(polygons, start=1):
        if not (
            isinstance(p, GeometryCollection)
            or isinstance(p, MultiPolygon)
            or isinstance(p, Polygon)
        ):
            continue
        if isinstance(p, GeometryCollection):
            polygons = get_polygons(p)
            for x in polygons:
                draw_polygon(x, subplot, text=str(i))
            continue
        draw_polygon(p, subplot, text=str(i))


def draw_this_moment(polygons, oil_map, grid=False, save_as=None):
    subplot = create_subplot_to_draw(oil_map, grid)
    draw_cells(oil_map, subplot)
    draw_polygons(polygons, subplot)
    if save_as is not None:
        figures_dir = 'figures'
        if not os.path.isdir(figures_dir):
            os.mkdir(figures_dir)
        pyplot.savefig(os.path.join(figures_dir, save_as))
    else:
        pyplot.show()
    pyplot.close()
