from minimizer import OilMap


def test_oil_map_values():
    oil_map = OilMap('test_files/oil0.txt', 'test_files/poro0.txt')
    actual = [[cell.oil for cell in row] for row in oil_map.matrix]
    assert actual == [[3.0, 0.0, 0.0], [1.0, 0.0, 2.0]]
    assert oil_map[0, 0].oil == 3.0
    assert oil_map[2, 1].oil == 2.0
    assert oil_map[0, 1].oil == 1.0
    assert oil_map[0, 0].poro == 1.0
    assert oil_map[2, 1].poro == 6.0
    assert oil_map[0, 1].poro == 4.0


def test_oil_map_areas():
    oil_map = OilMap('test_files/oil0.txt', 'test_files/poro0.txt')
    actual = [[cell.polygon.area for cell in row] for row in oil_map.matrix]
    assert actual == [[2500.0, 2500.0, 2500.0], [2500.0, 2500.0, 2500.0]]


def test_oil_map_cell_coordinates():
    oil_map = OilMap('test_files/oil0.txt', 'test_files/poro0.txt')
    # значения в bounds - это (x_min, y_min, x_max, y_max)
    assert oil_map[0, 0].polygon.bounds == (0.0, 0.0, 50.0, 50.0)
    assert oil_map[2, 0].polygon.bounds == (100.0, 0.0, 150.0, 50.0)
    assert oil_map[0, 1].polygon.bounds == (0.0, 50.0, 50.0, 100.0)
