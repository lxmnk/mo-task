import socket

import pytest

from minimizer import (
    OilMap,
    OilWell,
    create_offsprings,
    find_oil_wells_coordinates_using_ga,
    init_ga,
    make_natural_selection,
)


def test_init_ga():
    oil_map = OilMap('test_files/oil6.txt', 'test_files/poro6.txt')
    actual = init_ga(8, 100, oil_map)
    assert len(actual) == 8
    assert all([isinstance(x, OilWell) for x in actual])


def test_create_offsprings():
    oil_map = OilMap('test_files/oil6.txt', 'test_files/poro6.txt')
    oil_wells = [
        OilWell(150, 243, 100, oil_map),
        OilWell(760, 750, 100, oil_map),
        OilWell(838, 328, 100, oil_map),
        OilWell(821, 283, 100, oil_map),
    ]
    actual = create_offsprings(oil_wells, oil_map)
    assert len(actual) == 4
    assert all([isinstance(x, OilWell) for x in actual])
    assert all([x not in oil_wells for x in actual])


def test_make_natural_selection():
    oil_map = OilMap('test_files/oil6.txt', 'test_files/poro6.txt')
    oil_wells = [
        OilWell(150, 243, 100, oil_map),
        OilWell(760, 750, 100, oil_map),
        OilWell(838, 328, 100, oil_map),
        OilWell(821, 283, 100, oil_map),
    ]
    actual = make_natural_selection(oil_wells, 10, oil_map)
    assert len(actual) == 2


def test_ga_easy():
    oil_map = OilMap(
        'test_files/DATA/OIL_10.txt', 'test_files/DATA/PORO_10.txt'
    )
    find_oil_wells_coordinates_using_ga(
        8, 16, 10, oil_map, last_generation=16, logging=True, concurrency=False
    )


@pytest.mark.skipif(
    socket.gethostname().startswith('runner'),
    reason="Don't run heavy test on CI",
)
def test_ga_normal():
    oil_map = OilMap(
        'test_files/DATA/OIL_100.txt', 'test_files/DATA/PORO_100.txt'
    )
    find_oil_wells_coordinates_using_ga(
        16,
        16,
        50,
        oil_map,
        last_generation=16,
        logging=True,
        concurrency=False,
    )


@pytest.mark.skipif(
    socket.gethostname().startswith('runner'),
    reason="Don't run heavy test on CI",
)
def test_ga_hard():
    oil_map = OilMap(
        'test_files/DATA/OIL_100.txt', 'test_files/DATA/PORO_100.txt'
    )
    find_oil_wells_coordinates_using_ga(
        16,
        32,
        100,
        oil_map,
        last_generation=16,
        logging=True,
        concurrency=False,
    )


@pytest.mark.skipif(
    socket.gethostname().startswith('runner'),
    reason="Don't run heavy test on CI",
)
def test_ga_ultra():
    oil_map = OilMap(
        'test_files/DATA/OIL_300.txt', 'test_files/DATA/PORO_300.txt'
    )
    find_oil_wells_coordinates_using_ga(
        16,
        32,
        200,
        oil_map,
        last_generation=16,
        logging=True,
        concurrency=True,
    )
