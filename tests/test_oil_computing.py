from math import pi
from pprint import pprint

from pytest import approx

from minimizer import (
    OilMap,
    OilWell,
    OilWellsGroup,
    Point,
    get_circles_intersections,
    get_intersected_cells,
    get_oil_by_each_oil_well,
    get_oil_by_each_oil_well_for_all_iterations,
    get_overlapped_oil,
    get_total_oil_wells_oil,
)


def test_circle_inside_cell():
    oil_map = OilMap('test_files/oil1.txt', 'test_files/poro1.txt')
    r = 4
    circle = Point(55, 55).buffer(r)
    oil = get_overlapped_oil(circle, get_intersected_cells(circle, oil_map))
    expected = pi * r ** 2
    assert oil == approx(expected, rel=1e-2)


def test_cell_in_circle():
    oil_map = OilMap('test_files/oil1.txt', 'test_files/poro1.txt')
    r = 20
    circle = Point(55, 55).buffer(r)
    oil = get_overlapped_oil(circle, get_intersected_cells(circle, oil_map))
    expected = pi * r ** 2
    assert oil == approx(expected, rel=1e-2)


def test_by_Yanis_12_58():
    oil_map = OilMap('test_files/oil4.txt', 'test_files/poro4.txt')
    circle = Point(50.1, 50.1).buffer(30)
    oil = get_overlapped_oil(circle, get_intersected_cells(circle, oil_map))
    expected = 1514.23
    assert oil == approx(expected, rel=1e-2)
    pprint(oil)


def test_by_Yanis_13_04():
    oil_map = OilMap('test_files/oil3.txt', 'test_files/poro3.txt')
    circle = Point(50.1, 50.1).buffer(30)
    oil = get_overlapped_oil(circle, get_intersected_cells(circle, oil_map))
    expected = 1565.36
    assert oil == approx(expected, rel=1e-2)
    pprint(oil)


def test_by_Yanis_13_09():
    oil_map = OilMap('test_files/oil2.txt', 'test_files/poro2.txt')
    circle = Point(50.1, 50.1).buffer(30)
    oil = get_overlapped_oil(circle, get_intersected_cells(circle, oil_map))
    expected = 1316.15
    assert oil == approx(expected, rel=1e-2)
    pprint(oil)


def test_third_level_intersection():
    c1, c2 = Point(40, 50).buffer(20), Point(60, 50).buffer(20)
    c3, c4 = Point(50, 30).buffer(20), Point(80, 50).buffer(10)
    intersections = get_circles_intersections([c1, c2, c3, c4])
    expected = {
        frozenset({1}): 582.31,
        frozenset({2}): 442.54,
        frozenset({3}): 662.30,
        frozenset({4}): 173.88,
        frozenset({1, 2}): 262.30,
        frozenset({1, 3}): 182.31,
        frozenset({2, 3}): 182.31,
        frozenset({2, 4}): 139.78,
        frozenset({1, 2, 3}): 227.70,
    }
    for k in expected:
        assert expected[k] == approx(intersections[k].area, rel=1e-2)


def test_upper_level_intersection():
    oil_map = OilMap('test_files/oil1.txt', 'test_files/poro1.txt')
    c1, c2 = Point(50, 70).buffer(30), Point(40, 40).buffer(30)
    c3, c4 = Point(60, 30).buffer(20), Point(70, 50).buffer(20)
    c5, c6 = Point(80, 30).buffer(30), Point(40, 50).buffer(20)
    c7, c8 = Point(80, 60).buffer(30), Point(50, 30).buffer(20)
    circles = [c1, c2, c3, c4, c5, c6, c7, c8]
    expected = get_total_oil_wells_oil(circles, oil_map)
    actual = get_oil_by_each_oil_well(circles, oil_map, concurrency=False)
    assert expected == approx(sum(actual), rel=1e-2)
    print()
    pprint(actual)


def test_by_Vasekin_09_32():
    oil_map = OilMap('test_files/oil5.txt', 'test_files/poro5.txt')
    oil_well = OilWell(8950, 8950, 200, oil_map)
    oil_wells = OilWellsGroup([oil_well])
    actual = sum(oil_wells.get_oil(oil_map, concurrency=False))
    for _ in range(10):
        oil_wells.update()
        actual += sum(oil_wells.get_oil(oil_map, concurrency=False))
    expected = 442790242.218
    assert expected == approx(actual, rel=1e-2)
    print()
    pprint(actual)


def test_get_oil_by_each_oil_well_for_all_iterations():
    oil_map = OilMap('test_files/oil5.txt', 'test_files/poro5.txt')
    oil_wells = [
        OilWell(8950, 8950, 200, oil_map),
        OilWell(11000, 5000, 200, oil_map),
    ]
    actual = get_oil_by_each_oil_well_for_all_iterations(
        oil_wells, 10, oil_map, concurrency=False
    )
    print()
    pprint(actual)
